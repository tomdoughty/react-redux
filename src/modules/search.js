export const SEARCH_REQUESTED = 'search/SEARCH_REQUESTED'
export const SEARCH = 'search/SEARCH'

const initialState = {
    searchResults: null,
    isSearching: false
}

export default (state = initialState, action) => {
    switch (action.type) {
        case SEARCH_REQUESTED:
        return {
            ...state,
            isSearching: true
        }

        case SEARCH:
        return Object.assign({}, state, {
            searchResults: action.payload,
            isSearching: !state.isSearching
        })

        default:
        return state
    }
}

export const search = (term) => {
    return dispatch => {
        dispatch({
            type: SEARCH_REQUESTED
        })

        return fetch(`api/spotify/${term}`)
        .then((response) => {
            return response.json()
        })
        .then((json) => {
            dispatch({
                type: SEARCH,
                payload: json
            })
        })
    };
}
