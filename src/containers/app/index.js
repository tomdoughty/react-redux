import React from 'react';
import { Route, Link } from 'react-router-dom'
import Home from '../home'
import Info from '../info'

const App = () => (
    <div>
        <header>
            <nav>
                <ul>
                    <li><Link to="/">Home</Link></li>
                    <li><Link to="/info">Info</Link></li>
                </ul>
            </nav>
        </header>

        <main>
            <Route exact path="/" component={Home} />
            <Route exact path="/info" component={Info} />
        </main>
    </div>
)

export default App
