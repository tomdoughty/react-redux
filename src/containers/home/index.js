import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {
  search
} from '../../modules/search'

class Home extends React.Component {

    constructor(props) {
        super(props);
        this.handleForm = this.handleForm.bind(this);
    }

    handleForm(e) {
        e.preventDefault();
        this.props.search(this.refs.searchInput.value);
    }

    render() {
        return (
            <div>
                <h1>React, Redux, Thunks, Routing</h1>

                <form className="search" onSubmit={this.handleForm} >
                    <input ref="searchInput" />
                    <button type="submit">
                        {!this.props.isSearching ? 'Search' : 'Loading'}
                    </button>
                </form>

                { this.props.searchResults &&
                    <div className="results">
                        <ul>
                    		{ this.props.searchResults.tracks.items.map((track, index) => (
                                <li key={index}>
                                    <img src={ track.album.images[2].url } alt={track.album.name} />
                                    <div>
                                        <span><b>{ track.name }</b></span>
                                        <span>{ track.artists[0].name }</span>
                                    </div>
                                </li>
                    		)) }
                    	</ul>
                    </div>
                }

            </div>
        )
    }
}

const mapStateToProps = state => ({
    searchResults: state.search.searchResults,
    isSearching: state.search.isSearching
})

const mapDispatchToProps = dispatch => bindActionCreators({
    search
}, dispatch)

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Home)
