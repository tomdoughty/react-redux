var express    = require('express');
var bodyParser = require('body-parser');
var request    = require('request');

var app        = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 3001;

var router = express.Router();

var client_id = '661e8cd98375420f925728e0cd846506';
var client_secret = 'ab50f769326c4a4bafcb8d15a432cc8d';

var authOptions = {
  url: 'https://accounts.spotify.com/api/token',
  headers: {
    'Authorization': 'Basic ' + (new Buffer(client_id + ':' + client_secret).toString('base64'))
  },
  form: {
    grant_type: 'client_credentials'
  },
  json: true
};

router.route('/spotify/:query')
    .get(function(req, res) {
        let url = `https://api.spotify.com/v1/search?q=${req.params.query}&type=track`;
        request.post(authOptions, function(error, response, body) {
          if (!error && response.statusCode === 200) {
            var token = body.access_token;
            var options = {
              url: url,
              headers: {
                'Authorization': 'Bearer ' + token
              },
              json: true
            };
            request.get(options, function(error, response, body) {
                res.json(body);
            });
          }
        });
    });

app.use('/api', router);

app.listen(port);
